#include <HMC6352.h>

#include <Wire.h>


HMC6352 compass;
  
void setup()
{  
  Wire.begin();
  Serial.begin(9600);
  
  delay(100);
  Serial.println("RST");
  delay(100);  
}

void loop() 
{ 
  compass.wakeUp();
  Serial.println(compass.getHeading());
  compass.sleep();
  delay(1000);
}
