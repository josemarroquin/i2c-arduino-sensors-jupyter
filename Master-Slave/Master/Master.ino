#include <Wire.h>
#include <DHT11.h>
#include <HMC6352.h>

//Se debe tener cuidado con las transformaciones
const byte AnalogueInputPin = 0;
const byte AnalogueInputPin1 = 1;
const byte SlaveDeviceId = 1;
char ms1[150];
int pin=2;
DHT11 dht11(pin);
HMC6352 compass;


void setup()
{
  Wire.begin(); // Se inicia la coneccion i2c como master.
  pinMode(AnalogueInputPin, INPUT); 
  Serial.begin(9600);
}


void sensor_write(char *s)
{
  Wire.beginTransmission(SlaveDeviceId);//Se realiza conección el esclavo con id 1 
  Wire.write(s,300);
  Wire.endTransmission(); //Se cierra la transmision
 
}

void loop()
{
  float temp,hum;
  dht11.read(hum, temp);

  compass.wakeUp();
  float brujula = compass.getHeading();
  compass.sleep();
  
  //int input = analogRead(AnalogueInputPin); //Se lee un valor
  //Serial.print(input);
  
  //int input2 = analogRead(AnalogueInputPin1); //Se lee un valor
  //Serial.print(input2);
  
  //String message = "\nA0," + String(input) +"\nA3-I2C,"+ String(brujula)+"\nD21-I2C,"+String(hum);
  //String message2 = "\nD22-I2C,"+String(temp);

  String message= "\n"+String(brujula)+","+String(temp)+","+String(hum);
  //Serial.print(message);
  message.toCharArray(ms1,message.length());
  Serial.print(ms1);
  sensor_write(ms1);
  

  
  delay(1000);
}


