#include <Wire.h>


//Se debe tener cuidado con las transformaciones


const byte SlaveDeviceId = 1;
char message[150];

void setup()
{
  //Se inicia como slave con id 1
  Wire.begin(SlaveDeviceId); 
  //Se declara un metodo para cuando se resiva una conección
  Wire.onReceive(receiveCallback);
  
  //Se inicia el seria para verificar
  Serial.begin(9600);
}

void loop()
{
}

//aCount numero de byte recibidos  
void receiveCallback(int aCount)
{
  int i = 0;
  while(0 < Wire.available())
  {
    message[i] = Wire.read();
    i++;
    
  }
  Serial.print(message);
}
